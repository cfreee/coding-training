function draw(data, id) {
    const sleep = (milliseconds) => {
            return new Promise(resolve => setTimeout(resolve, milliseconds))
        }
        //extract CSE scholars
    var nodes = [];
    var links = [];
    inCSE = id => {
        for (var i = 0; i < nodes.length; i++) {
            if (nodes[i].id == id && nodes[i].dept == "CSE")
                return true;
        }
        return false;
    }
    data.nodes.forEach(d => { if (d.dept == "CSE") nodes.push(d); })
    data.edges.forEach(d => { if (inCSE(d.source) && inCSE(d.target)) links.push(d); })
        // options
    var orderBy = "random";
    const select = d3.select(id).append("select")
        .on("change", reorder)
    const options = select.selectAll("option")
        .data(["random", "name", "publication"]).enter()
        .append("option")
        .text(d => d);
    // svg attribute
    const width = height = Math.max(300, 12 * nodes.length);
    const margin = { "up": 100, "left": 100, "down": 100, "right": 100 };
    const svg = d3.select(id).append('svg')
        .attr("viewBox", [-margin.left, -margin.up, width + margin.up + margin.down, height + margin.left + margin.right])
        .attr("preserveAspectRatio", "xMidYMid meet")
        .attr("style", "height: 100%;");
    // Force-directed graph layout
    const r = 4;
    const fdColor = d3.scaleOrdinal(d3.schemeCategory10);
    const simulation = forceSimulation(nodes, links).on("tick", ticked);
    nodes.forEach(d => { d.publicationNum = 0 })
    links.forEach(d => {
        d.source.publicationNum += d.publications.length;
        d.target.publicationNum += d.publications.length;
    })
    const rScale = d3.scaleLinear()
        .domain([0, Math.max.apply(Math, nodes.map(d => d.publicationNum))])
        .range([5, 15])
    nodes.forEach(d => { d.r = rScale(d.publicationNum) })
    console.log(nodes.map(d => d.r))
        // Matrix layout 
    const cellWidth = 10;
    const cellPadding = 2;
    const x = d3.scaleBand()
        .domain(d3.range(0, nodes.length))
        .range([0, width]);
    const y = d3.scaleBand()
        .domain(d3.range(0, nodes.length))
        .range([0, width]);
    const maxNum = Math.max.apply(Math, links.map(d => d.publications.length));
    const cellColor = d3.scaleSequential(d3.interpolateRdYlBu)
        .domain([maxNum, 0]);
    // View switch
    var mode = "force_directed";

    function fd2m() {
        svg.selectAll(".link")
            .transition().duration(2000)
            .attr("points", d => {
                return [
                    [x(d.target.orderList[orderBy]), y(d.source.orderList[orderBy])],
                    [x(d.target.orderList[orderBy]) + cellWidth, y(d.source.orderList[orderBy])],
                    [x(d.target.orderList[orderBy]) + cellWidth, y(d.source.orderList[orderBy]) + cellWidth],
                    [x(d.target.orderList[orderBy]), y(d.source.orderList[orderBy]) + cellWidth]
                ]
            })
            .style("fill", d => cellColor(d.publications.length));
        svg.selectAll(".temp_cell")
            .attr("points", d => {
                return [
                    [x(d.target.orderList[orderBy]), y(d.source.orderList[orderBy])],
                    [x(d.target.orderList[orderBy]) + cellWidth, y(d.source.orderList[orderBy])],
                    [x(d.target.orderList[orderBy]) + cellWidth, y(d.source.orderList[orderBy]) + cellWidth],
                    [x(d.target.orderList[orderBy]), y(d.source.orderList[orderBy]) + cellWidth]
                ]
            })
            .transition().delay(2000)
            .attr("display", "block")
            .transition()
            .duration(1000)
            .attr("points", d => {
                return [
                    [x(d.source.orderList[orderBy]), y(d.target.orderList[orderBy])],
                    [x(d.source.orderList[orderBy]) + cellWidth, y(d.target.orderList[orderBy])],
                    [x(d.source.orderList[orderBy]) + cellWidth, y(d.target.orderList[orderBy]) + cellWidth],
                    [x(d.source.orderList[orderBy]), y(d.target.orderList[orderBy]) + cellWidth]
                ]
            })
        sleep(3000).then(() => {
            svg.selectAll(".matrix")
                .attr("display", "block");
            mode = "matrix";
        });
    }

    function m2fd() {
        svg.selectAll(".link")
            .transition()
            .duration(2500)
            .attr("points", d => [
                [d.source.x, d.source.y],
                [d.source.x, d.source.y],
                [d.target.x, d.target.y],
                [d.target.x, d.target.y]
            ])
            .style("fill", "#999");
        sleep(2500).then(() => {
            svg.selectAll(".fd")
                .attr("display", "block");
            mode = "force_directed";
            ticked();
        });
    }
    svg.on("click", () => {
        if (mode == "switch") return;
        var pastMode = mode;
        mode = "switch";
        clear();
        if (pastMode == "force_directed") fd2m();
        else m2fd();
    })

    function fdInit() {
        //common component form morphing
        const link = svg.append("g")
            .attr("class", "links")
            .selectAll("line")
            .data(links)
            .enter().append("polygon")
            .attr("class", "link")
            .style("stroke-width", 1)
            .style("stroke", "#999")
            .style("stroke-opacity", 0.6);

        const fdg = svg.append("g")
            .attr("class", "fd")

        const node_group = fdg.selectAll(".node")
            .data(nodes)
            .enter()
            .append("g")
            .attr("class", "node");

        const node = node_group.append("circle")
            .attr("class", "node_circle")
            .attr("r", d => d.r)
            .attr("fill", d => fdColor(Math.random()))
    }

    function ticked() {
        if (mode == "force_directed") {
            svg.selectAll(".node_circle")
                .attr("cx", d => d.x = Math.max(r, Math.min(width - r, d.x)))
                .attr("cy", d => d.y = Math.max(r, Math.min(height - r, d.y)));
            svg.selectAll(".link")
                .attr("points", d => [
                    [d.source.x, d.source.y],
                    [d.source.x, d.source.y],
                    [d.target.x, d.target.y],
                    [d.target.x, d.target.y]
                ])
        }
    }

    function clear() {
        svg.selectAll(".fd")
            .attr("display", "none");
        svg.selectAll(".matrix")
            .attr("display", "none");
        svg.selectAll(".temp_cell")
            .attr("display", "none");
        svg.select("select")
            .attr("display", "none");
    }

    function forceSimulation(nodes, links) {
        return d3.forceSimulation(nodes)
            .force("link", d3.forceLink(links).id(d => d.id))
            .force("charge", d3.forceManyBody().strength(-100))
            .force("center", d3.forceCenter(width / 2, height / 2))
    }

    function matrixInit() {
        // data preparison
        const fullLinks = [].concat.apply([], nodes.map(n1 => nodes.map(n2 => { return { "source": n1, "target": n2 }; })));
        // node order list init
        for (var i = 0; i < nodes.length; i++) {
            nodes[i].orderList = {};
            nodes[i].order = i;
        }
        // random order
        for (var i = 0; i < nodes.length; i++) {
            nodes[i].orderList.random = i;
        }
        // name order
        nodes.sort((a, b) => {
            if (a.fullname < b.fullname) return -1;
            else if (a.fullname > b.fullname) return 1;
            else return 0;
        })
        for (var i = 0; i < nodes.length; i++) {
            nodes[i].orderList.name = i;
        }
        // coorperated publication number order
        nodes.sort((a, b) => {
            if (a.publicationNum > b.publicationNum) return -1;
            else if (a.publicationNum < b.publicationNum) return 1;
            else return 0;
        })
        for (var i = 0; i < nodes.length; i++) {
            nodes[i].orderList.publication = i;
        }
        //console.log(nodes);
        const matrixg = svg.append("g")
            .attr("class", "matrix")
        const row = matrixg.append("g")
            .attr("class", "row")
            .selectAll("author_row")
            .data(nodes).enter()
            .append("g")
            .attr("class", "author_row")
            .attr("transform", d => { return "translate(0," + y(d.orderList[orderBy]) + ")" });
        const textShrinkage = d => {
            const maxLength = 18;
            if (d.fullname.length > maxLength) {
                return [].concat.apply([], d.fullname.substring(0, maxLength).split(",").slice(0, this.length - 1)) + ",.."
            } else {
                return d.fullname;
            }
        }
        row.append("text")
            .text(textShrinkage)
            .attr("dy", cellWidth)
            .attr("dx", -2)
            .style("font-size", "10px")
            .style("text-anchor", "end");
        const column = matrixg.append("g")
            .attr("class", "column")
            .attr("transform", "translate(0, -5)")
            .selectAll("author_column")
            .data(nodes).enter()
            .append("g")
            .attr("class", "author_column")
            .attr("transform", d => "translate(" + x(d.orderList[orderBy]) + ",0) rotate(-90)");
        column.append("text")
            .text(textShrinkage)
            .attr("dy", cellWidth)
            .style("font-size", "10px")
        const cells = matrixg.append("g")
            .attr("class", "cells")
            .selectAll("cell")
            .data(fullLinks).enter()
            .append("g")
            .attr("class", "cell")
            .attr("transform", d => "translate(" + x(d.target.orderList[orderBy]) + "," + y(d.source.orderList[orderBy]) + ")");
        cells.append("rect")
            .attr("width", cellWidth)
            .attr("height", cellWidth)
            .style("fill", "#fff")
            .style("fill", "#eee")
            .style("opacity", 0.6)

        svg.append("g")
            .attr("class", "temp")
            .selectAll(".temp_cell")
            .data(links).enter()
            .append("polygon")
            .attr("class", "temp_cell")
            .style("stroke-width", 1)
            .style("stroke", "#999")
            .style("stroke-opacity", 0.6)
            .style("fill", d => cellColor(d.publications.length));

        const colorLegend = group => {
            const cell_size = 10;
            const legend_cell = group.selectAll(".legend_cells")
                .data(d3.range(10).map(d => [maxNum - (d + 1) / 10 * maxNum, maxNum - d / 10 * maxNum]))
                .enter()
                .append("g")
                .attr("class", "legend_cells")
                .attr("transform", (d, i) => "translate(0, " + i * cell_size + ")");
            legend_cell.append("rect")
                .attr("width", cell_size)
                .attr("height", cell_size)
                .style("fill", d => cellColor(d[0]))
            legend_cell.append("text")
                .attr("dx", 1.5 * cell_size)
                .attr("dy", 0.8 * cell_size)
                .text(d => "" + d[0].toFixed(0) + " to " + d[1].toFixed(0))
                .style("font-size", cell_size * 0.8);
        }
        matrixg.append("g")
            .attr("transform", "translate( " + width + ", 10)")
            .call(colorLegend);
    }

    function reorder() {
        orderBy = d3.select('select').property('value');
        nodes.forEach(node => { node.order = node.orderList[orderBy] });
        if (mode == "matrix") {
            d3.selectAll(".author_row")
                .transition().duration(1000)
                .attr("transform", d => { return "translate(0," + y(d.orderList[orderBy]) + ")" });
            d3.selectAll(".author_column")
                .transition().duration(1000)
                .attr("transform", d => "translate(" + x(d.orderList[orderBy]) + ",0) rotate(-90)");
            d3.selectAll(".cell")
                .transition().duration(1000)
                .attr("transform", d => "translate(" + x(d.target.orderList[orderBy]) + "," + y(d.source.orderList[orderBy]) + ")");
            svg.selectAll(".link")
                .transition().duration(1000)
                .attr("points", d => {
                    return [
                        [x(d.target.orderList[orderBy]), y(d.source.orderList[orderBy])],
                        [x(d.target.orderList[orderBy]) + cellWidth, y(d.source.orderList[orderBy])],
                        [x(d.target.orderList[orderBy]) + cellWidth, y(d.source.orderList[orderBy]) + cellWidth],
                        [x(d.target.orderList[orderBy]), y(d.source.orderList[orderBy]) + cellWidth]
                    ]
                })
            svg.selectAll(".temp_cell")
                .transition().duration(1000)
                .attr("points", d => {
                    return [
                        [x(d.source.orderList[orderBy]), y(d.target.orderList[orderBy])],
                        [x(d.source.orderList[orderBy]) + cellWidth, y(d.target.orderList[orderBy])],
                        [x(d.source.orderList[orderBy]) + cellWidth, y(d.target.orderList[orderBy]) + cellWidth],
                        [x(d.source.orderList[orderBy]), y(d.target.orderList[orderBy]) + cellWidth]
                    ]
                })

        }

    };

    function init() {
        if (mode == "matrix") {
            fdInit();
            clear();
            matrixInit();
        } else {
            mode = "force_directed";
            matrixInit();
            clear();
            fdInit();
        }
    }
    init();
}