function drawMap(data, id) {
    // backet the daily data by month
    const mon = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var backetData = [];
    data.forEach(item => {
        // string parser
        var date = item.date.split("-");
        item.year = parseInt(date[0]);
        item.month = parseInt(date[1]);
        item.day = parseInt(date[2]);
        item.min_temperature = parseFloat(item.min_temperature);
        item.max_temperature = parseFloat(item.max_temperature);
        // backet the data item
        var backetId = (item.year << 4) + item.month; // 12 < 2^4
        var backetIndex = backetData.findIndex(backet => backet.id == backetId);
        if (-1 == backetIndex) {
            var backet = { "id": backetId, "item": [item], "dailyMax": Array(31), "dailyMin": Array(31) }
            backetData.push(backet);
        } else {
            backetData[backetIndex].item.push(item);
        }
    })
    backetData.forEach(backet => {
        backet.year = backet.id >> 4;
        backet.month = mon[(backet.id % 16) - 1];
        backet.max = Math.max.apply(Math, backet.item.map(d => d.max_temperature));
        backet.min = Math.min.apply(Math, backet.item.map(d => d.min_temperature));
        backet.item.forEach(d => { backet.dailyMax[d.day - 1] = d.max_temperature, backet.dailyMin[d.day - 1] = d.min_temperature })
    })

    const minYear = Math.min.apply(Math, backetData.map(d => d.year));
    const maxYear = Math.max.apply(Math, backetData.map(d => d.year));
    const rectWidth = 30;
    const rectHeight = 30;
    const margin = { "top": 20, "left": 60, "right": 80 }; //margin of the main part
    const rectPadding = { "top": 5, "left": 5 };
    const height = (rectHeight + 2 * rectPadding.top) * 12;
    const width = (rectWidth + 2 * rectPadding.left) * (maxYear - minYear + 1);
    const svg = d3.select(id).append("svg")
        .attr("viewBox", [-margin.left, -margin.top, width + margin.left + margin.right, height + margin.top])
        .attr("preserveAspectRatio", "xMinYMin meet")
        .attr("style", "height: 100%;");
    const x = d3.scaleBand()
        .domain(d3.range(minYear, maxYear + 1, 1))
        .range([0, width]);
    const y = d3.scaleBand()
        .domain(mon)
        .range([0, height]);
    const maxTemp = Math.max.apply(Math, backetData.map(d => d.max));
    const minTemp = Math.min.apply(Math, backetData.map(d => d.min));
    const color = d3.scaleSequential(d3.interpolateRdYlBu)
        .domain([maxTemp, minTemp]);
    const lineY = d3.scaleLinear()
        .domain([minTemp, maxTemp])
        .range([0.9 * rectHeight, 0.1 * rectHeight])
    const lineX = d3.scaleLinear()
        .domain([0, 30])
        .range([0, rectWidth])
    svg.append("g").call(d3.axisTop(x));
    svg.append("g").call(d3.axisLeft(y));
    var maxmin = "max";
    const rects = svg.selectAll("rect")
        .data(backetData)
        .enter()
        .append("g")
        .attr("transform", d => "translate(" + (x(d.year) + rectPadding.left) + ", " + (y(d.month) + rectPadding.top) + ")");
    // click the svg for transition between min/max
    const trans = () => {
        maxmin = maxmin == "max" ? "min" : "max";
        svg.selectAll(".rect")
            .transition()
            .style("fill", d => color(d[maxmin]));
    }
    svg.on("click", trans);
    rects.on()
    rects.append("rect")
        .attr("class", "rect")
        .attr("width", rectWidth)
        .attr("height", rectHeight)
        .style("fill", d => color(d[maxmin]));

    // tooltip
    const tooltip = d3.select(id)
        .append("div")
        .style("position", "absolute")
        .style("z-index", "10")
        .style("visibility", "hidden")
        .text("default");
    rects.on("mouseover", () => tooltip.style("visibility", "visible"))
        .on("mousemove", d => {
            tooltip.style("top", (event.pageY - 10) + "px").style("left", (event.pageX + 10) + "px");
            tooltip.text(d.year + "-" + d.month.substr(0, 3) + " max: " + d.max + " min: " + d.min)
        })
        .on("mouseout", () => tooltip.style("visibility", "hidden"));


    ineGenerator = d3.line()
        .x((d, i) => lineX(i))
        .y(d => lineY(d));
    // Line generator from d3 lib cannot handle data missing
    const robustLineGenerator = d => {
            var link = false;
            var path = "";
            for (var i = 0; i < d.length; i++) {
                if (typeof d[i] !== 'undefined') {
                    if (link) {
                        path += ("L" + lineX(i) + "," + lineY(d[i]));
                    } else {
                        path += ("M" + lineX(i) + "," + lineY(d[i]));
                    }
                    link = true;
                } else {
                    link = false;
                }
            }
            return path;
        }
        //line chart of max-temp and min-temp
    rects.append("path")
        .attr("class", "max_line")
        .datum(d => d.dailyMax)
        .attr("d", robustLineGenerator)
        .style("stroke", "#64b3f4")
        .style("fill", "none")
        .style("opacity", 0.8);
    rects.append("path")
        .attr("class", "min_line")
        .datum(d => d.dailyMin)
        .attr("d", robustLineGenerator)
        .style("stroke", "#c2e59c")
        .style("fill", "none")
        .style("opacity", 0.8);

    // legend
    // colorLegend from d3-legend v4 cannot handle sequantialScale
    const colorLegend = group => {
        const cell_size = 10;
        const legend_cell = group.selectAll(".legend_cells")
            .data(d3.range(10).map(d => maxTemp - d / 10 * (maxTemp - minTemp)))
            .enter()
            .append("g")
            .attr("class", "legend_cells")
            .attr("transform", (d, i) => "translate(0, " + i * cell_size + ")");
        legend_cell.append("rect")
            .attr("width", cell_size)
            .attr("height", cell_size)
            .style("fill", d => color(d))
        legend_cell.append("text")
            .attr("dx", 1.5 * cell_size)
            .attr("dy", 0.8 * cell_size)
            .text(d => d.toFixed(2))
            .style("font-size", cell_size * 0.8);
    }
    svg.append("g")
        .attr("transform", "translate( " + width + ", 10)")
        .call(colorLegend);
}